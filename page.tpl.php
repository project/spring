<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<head>
	<title><?php print $head_title ?><?php if ($site_slogan != '' && !$is_front) print ' &ndash; '. $site_slogan; ?></title>
	<meta http-equiv="content-language" content="<?php print $language ?>" />
	<?php print $meta; ?>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if IE]>
    <link rel="stylesheet" href="<?php print $path; ?>blueprint/blueprint/ie.css" type="text/css" media="screen, projection">
  	<link href="<?php print $path; ?>css/ie.css" rel="stylesheet"  type="text/css"  media="screen, projection" />
  <![endif]-->
</head>

<body class="<?php print $body_class; ?>">

<div class="container">
  <div class="column prepend-1 span-23 last">  
    <h1 id="logo">
      <a title="<?php print $site_name; ?><?php if ($site_slogan != '') print ' &ndash; '. $site_slogan; ?>" href="<?php print url(); ?>"><?php print $site_name; ?><?php if ($site_slogan != '') print ' &ndash; '. $site_slogan; ?></a>
    </h1>                            
    <h2 id="slogan" title="a blog about drupal, macs, productivity, health, and bmws">a blog about drupal, macs, productivity, health, and bmws</h2>
    <?php print $search; ?>
  </div>

  <ul id="nav">
    <li><?php print l('blog', 'node'); ?></li>
    <li><?php print l('projects', 'projects'); ?></li>
    <li><?php print l('photos', 'photos'); ?></li>
    <li><?php print l('about', 'node/47'); ?></li>                
    <li><?php print l('contact', 'contact'); ?></li>    
  </ul>

  <?php print $left; ?>

  <div class="<?php print $center; ?>">
    <?php
      if ($breadcrumb != '') {
        print $breadcrumb;
      }

      if ($tabs != '') {
        print '<div class="tabs">'. $tabs .'</div>';
      }

      if ($messages != '') {
        print '<div id="messages">'. $messages .'</div>';
      }
      
      if ($title != '') {
        print '<h2>'. $title .'</h2>';
      }      

      print $help; // Drupal already wraps this one in a class      

      print $content;
      print $feed_icons;
    ?>
  </div>

  <?php print $right; ?>
  
  <div class="column span-24 last">
    <div id="footer">
    <ul id="footer-nav">
      <li><?php print l('blog', 'node'); ?></li>
      <li><?php print l('projects', 'projects'); ?></li>
      <li><?php print l('photos', 'photos'); ?></li>
      <li><?php print l('about', 'node/47'); ?></li>                
      <li><?php print l('contact', 'contact'); ?></li>    
    </ul>      
      Code examples, code snippets, and downloadable zip files of code are licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/">Creative Commons License</a>.
      <br />All other content, unless where noted, &copy;<?php print date('Y'); ?> Theodore Serbinski. All Rights Reserved.</div>  
  </div>  

  <?php print $scripts ?>
  <?php print $closure; ?>

</div>

</body>
</html>

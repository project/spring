<div class="slider-wrap">
	<div id="slider1" class="csw">
		<div class="panelContainer">
			<div class="panel" title="MothersClick">
				<div class="wrapper">
          <a class="project-posts" href="<?php print url('tags/mothersclick'); ?>">See my blog posts for this project »</a>				  
					<h3>MothersClick</h3>
					<div class="column span-8">
					  <p>I joined <a href="http://www.mothersclick.com">MothersClick</a> in August of 2006 as the Chief Technology Officer for the company.</p>
					  <p>Since then I have led the architecture & development of the site through 3 major revisions. Additionally, we have spun off sister sites that are running on our powerful shared multisite platform.</p>
					</div>
          <div class="column span-5">
            <a href="http://www.mothersclick.com"><img class="img-right" src="<?php print $path; ?>images/logo-mc.png" /></a>
            <a href="http://www.parentsclick.net"><img class="img-right" src="<?php print $path; ?>images/logo-pcn.png" /></a>
            <a href="http://www.momblognetwork.com"><img class="img-right" src="<?php print $path; ?>images/logo-mbn.png" /></a>                        
          </div>
				</div>
			</div>
			<div class="panel" title="2 Guys Uncorked">
				<div class="wrapper">
          <a class="project-posts" href="<?php print url('tags/2-guys-uncorked'); ?>">See my blog posts for this project »</a>				  
					<h3>2 Guys Uncorked</h3>
          <a href="http://2guysuncorked.com"><img class="img-right" src="<?php print $path; ?>images/logo-2gu.png" /></a>					
					<p>In the summer of 2007, my good friend Jon and got together and decided to create a wine website, <a href="http://2guysuncorked.com">2 Guys Uncorked</a>, that made wine fun and easy, cutting out the pretentiousness often associated with other wine websites. You can <a href="http://2guysuncorked.com/about">read more about 2 Guys Uncorked here</a>.</p>
					<p>This website has a custom wine management system in the backend that we use to manage and track our wine reviews. This is important because Jon is in San Francisco and I'm in Washington, DC, and we need a way to track who has reviewed each wine, who needs to buy a bottle of the wine, and when to publish the review after it's been edited.</p>
					<p>Coming soon is an iPhone interface to the site that will make it super easy to figure out what wines to buy when you're at the wine store, stay tuned!</a>
				</div>
			</div>			
			<div class="panel" title="Drupal">
				<div class="wrapper">
          <a class="project-posts" href="<?php print url('tags/drupal'); ?>">See my blog posts for this project »</a>				  
					<h3>Drupal</h3>
          <a href="http://association.drupal.org/user/709"><img class="img-right" src="http://drupal.org/files/images/DA-individual-120.png" /></a>
					<p><a href="http://drupal.org/user/12932">I came to the Drupal project a little over 3 years ago</a> and immediately realized the potential of Drupal as a web framework for my projects. Jumping into the community, I start <a href="http://www.google.com/search?q=site%3Ahttp%3A%2F%2Fdrupal.org+m3avrck&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:en-US:official">contributing back patches, modules, and more</a> right away. Here's just a sampler list highlighting some of my larger interests...</p>
				  <h4>Current interests</h4>
			    <ul>
					  <li><a href="http://drupal.org/project/blueprint">Blueprint theme</a></li>
					  <li><a href="http://drupal.org/project/simplefeed">SimpleFeed module</a></li>
					  <li><a href="http://drupal.org/project/simplemenu">SimpleMenu module</a></li>
					  <li><a href="http://drupal.org/project/elf">External links filter (ELF) module</a></li>
				  </ul>
  				<h4>Previous interests</h4>
          <ul>
					  <li><a href="http://drupal.org/project/tinymce">TinyMCE module</a></li>
					  <li><a href="http://drupal.org/project/scheduler">Scheduler module</a></li>
					  <li><a href="http://drupal.org/project/timesheet">Timesheet module</a></li>
					</ul>
				</div>
			</div>		
			<div class="panel" title="BMW M3">
				<div class="wrapper">
          <a class="project-posts" href="<?php print url('tags/bmw'); ?>">See my blog posts for this project »</a>
					<h3>BMW M3</h3>					
          <a href="http://www.flickr.com/photos/tedserbinski/183083861/" title="DSC_7740_FR.JPG by m3avrck, on Flickr"><img class="img-right" src="http://farm1.static.flickr.com/23/183083861_19d58061c7_m.jpg" width="240" height="159" /></a>          
          <p>When I'm not sitting at my desk, I love to be out on the open road, driving, and occasionally, racing my BMW M3. What's an M3 you ask? Well probably one of the best cars ever built, in my opinion that is :-) For way too many details, <a href="http://www.bmwmregistry.com/model_faq.php?id=15">this is an excellent resource about my particular M3.</a></p>
          <p>Below is a list of some of the best resources on the BMW M3 that I have compiled. At the top is my maintenance log for my car, always a handy thing to have for anyone working on a car :)</p>
         	<ul>
         		<li><a href="<?php print url('m3/log'); ?>">My M3 Log</a></li>
         		<li><a href="http://forums.bimmerforums.com/forum/showthread.php?t=265292">Oil FAQ</a></li>
         		<li><a href="<?php print base_path(); ?>files/e36m3faq.pdf">E36 M3 FAQ</a></li>
         		<li><a href="<?php print base_path(); ?>files/buing_an_e36_m3.doc">Buying an E36 M3 FAQ</a></li>
         		<li><a href="<?php print base_path(); ?>files/e36lighting.pdf">E36 Lighting</a></li>
         		<li><a href="<?php print base_path(); ?>files/bmw_torq.pdf">BMW Torque Spec Reference</a></li>
         		<li><a href="http://www.fortune3.com/comp73891/?pagesuffix=e3675k.html&">E36 75k Check</a></li>
         	</ul>    

          <p>And of course, <a href="http://www.flickr.com/photos/tedserbinski/tags/m3/">obligatory pictures of my car</a>.</p>          
				</div>
			</div>
		</div><!-- .panelContainer -->
	</div><!-- #slider1 -->
</div><!-- .slider-wrap -->

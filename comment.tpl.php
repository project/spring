<div class="clear-block comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; if ($author_comment) print ' author'; print ' '. $zebra; ?>">

  <div class="picture span-3">
    <?php print $picture ?>
  </div>

  <div class="comment-content">
    <?php if ($submitted): ?>
      <span class="username"><?php print theme('username', $comment); ?></span> <span class="date"><?php print t('wrote !date ago', array( '!date' => format_interval(time() - $comment->timestamp))); ?></span>
    <?php endif; ?>

    <?php if ($comment->new) : ?>
      <a id="new"></a>
      <span class="new"><?php print drupal_ucfirst($new) ?></span>
    <?php endif; ?>
    <div class="content">
      <?php if ($title): ?><h3><?php print $title; ?></h3><? endif; ?>
      <?php print $content ?>
    </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links ?></div>
    <?php endif; ?>
  </div>
</div>
